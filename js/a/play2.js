var ScenePlay = new Phaser.Class({
	Extends: Phaser.Scene,

	initialize:
//=====INIT=====//
	// Constructor
	function ScenePlay(){
		Phaser.Scene.call(this, {key:'sceneplay'});
	},

	preload: function(){
		// 
		this.load.image('BG', 'assets/gfx/road.png');
		this.load.image('BMulai', 'assets/gfx/play1.png');
		this.load.image('Halangan', 'assets/gfx/titik_black.png');
		this.load.image('FG_Awal', 'assets/gfx/setting.png');
		this.load.image('Title', 'assets/gfx/Title.png');
		this.load.image('Chara', 'assets/gfx/Chara.png');
		this.load.image('Chara1', 'assets/gfx/Chara1.png');
		this.load.image('Chara2', 'assets/gfx/Chara2.png');
		this.load.image('Mobil', 'assets/gfx/halangan.png');

		
		this.clicking = false;
		this.isGameRunning = false;

		this.timer_halangan = 0;
		this.halangan = [];

	},
	startGame: function(){
		this.tweens.add({
			targets: this.FG_Awal,
			ease: "Power 1",
			duration: 500,
			y: 768 / 2 - 768
		})
		this.tweens.add({
			targets: this.BMulai,
			ease: 'Back.easeIn',
			duration: 750,
			scaleX: 0,
			scaleY: 0
		});
		this.tweens.add({
			targets: this.Title,
			ease: 'Elastic',
			duration: 750,
			scaleX: 0,
			scaleY: 0
		});
	
		
		this.Chara.setVisible(true);
		this.Chara1.setVisible(true);
		this.Chara2.setVisible(true);

	},
		karakter: function(){
		this.Chara.setPosition (50, 500);
		this.Chara.setVisible(true);
		this.Chara.setScale(1);

		this.tweens.add({
		targets: this.Chara,
		duration: 11000,
		y: 768 / 2 - 768
		 });

		},

		karakter1: function(){
		this.Chara1.setPosition (200, 500);
		 this.Chara1.setVisible(true);
		 this.Chara1.setScale(1);

		 this.tweens.add({
		 	targets: this.Chara1,
		 	duration: 11000,
		 	y: 768 / 2 - 768
		 });
		},

		karakter2: function(){
		this.Chara2.setPosition(350, 500);
		this.Chara2.setVisible(true);
		this.Chara2.setScale(1);

		this.tweens.add({
		 	targets: this.Chara2,
		 	duration: 11000,
		 	y: 768 / 2 - 768
		 });
		 // this.isGameRunning = true;
		},
//=====#INIT=====//
	
//=====EVENT LISTENER=====//
	onObjectClick:function(pointer,gameObject){
		console.log("Object Click");
		if (!this.isGameRunning && gameObject == this.BMulai)
		{
			this.BMulai.setTint(0x616161);
			this.clicking = true;
		}
		if (!this.isGameRunning && gameObject == this.Chara)
		{
			this.Chara.setTint(0x616161);
			this.clicking = true;
		}
		if (!this.isGameRunning && gameObject == this.Chara1)
		{
			this.Chara1.setTint(0x616161);
			this.clicking = true;
		}
		if (!this.isGameRunning && gameObject == this.Chara2)
		{
			this.Chara2.setTint(0x616161);
			this.clicking = true;
		}
		// 
	},
	onObjectOver:function(pointer,gameObject){
		console.log("Object Over");
		if(!this.clicking)return;
		if(!this.isGameRunning && gameObject == this.BMulai)
		{
			this.BMulai.setTint(0x616161);
		}
		if(!this.clicking)return;
		if(!this.isGameRunning && gameObject == this.Chara)
		{
			this.Chara.setTint(0x616161);
		}
		if(!this.clicking)return;
		if(!this.isGameRunning && gameObject == this.Chara1)
		{
			this.Chara1.setTint(0x616161);
		}
		if(!this.clicking)return;
		if(!this.isGameRunning && gameObject == this.Chara2)
		{
			this.Chara2.setTint(0x616161);
		}

		// 
	},
	onObjectOut:function(pointer,gameObject){
		console.log("Object Out");
		if(!this.clicking)return;
		if(!this.isGameRunning && gameObject == this.BMulai)
		{
			this.BMulai.setTint(0xffffff);
		}
		if(!this.clicking)return;
		if(!this.isGameRunning && gameObject == this.Chara)
		{
			this.Chara.setTint(0xffffff);
		}
		if(!this.clicking)return;
		if(!this.isGameRunning && gameObject == this.Chara1)
		{
			this.Chara1.setTint(0xffffff);
		}
		if(!this.clicking)return;
		if(!this.isGameRunning && gameObject == this.Chara2)
		{
			this.Chara2.setTint(0xffffff);
		}
		// 
	},
	onObjectClickEnd:function(pointer,gameObject){
		console.log("Object End Click");
		if(!this.isGameRunning && gameObject == this.BMulai)
		{
			this.BMulai.setTint(0xffffff);
			this.startGame();
		}
		if(!this.isGameRunning && gameObject == this.Chara)
		{
			this.Chara.setTint(0xffffff);
			this.karakter();
		}
		if(!this.isGameRunning && gameObject == this.Chara1)
		{
			this.Chara1.setTint(0xffffff);
			this.karakter1();
		}
		if(!this.isGameRunning && gameObject == this.Chara2)
		{
			this.Chara2.setTint(0xffffff);
			this.karakter2();
		}
		// 
		// 
	},
	onPointerUp:function(pointer,currentlyOver){
		console.log("Mouse Up");
		this.clicking = false;
		// 
	},
	startInputEvent: function(){
		this.input.on('pointerup', this.onPointerUp, this);
		this.input.on('gameobjectdown', this.onObjectClick, this);
		this.input.on('gameobjectup', this.onObjectClickEnd, this);
		this.input.on('gameobjectover', this.onObjectOver, this);
		this.input.on('gameobjectout', this.onObjectOut, this);
	},
	 update: function(){
		if (this.timer_halangan == 0)
		{
		var acak_y = Math.floor((Math.random()*438)+50);	
		var mobil = this.add.image(768/2,'Mobil');
		mobil.setOrigin(0.0);
		mobil.setData("status_aktif", true);
		mobil.setData("kecepatan", Math.floor((Math.random()*15)+10))
		mobil.setDepth(5);

		this.halangan.push(mobil);
		this.timer_halangan = Math.floor((Math.random()*50)+10);
	 }
	 for (var i = this.halangan.length -1; i>=0; i--)
	 {
	 	this.halangan[i].x -= this.halangan[i].getData("kecepatan");
	 	if(this.halangan[i].x <-200)
	 	{
	 		this.halangan[i].destroy();
	 		this.halangan.splice(i,1);
	 	}
	 	this.timer_halangan--;
	 }
		},

	create: function(){
		this.add.image(432/2,768/2, 'BG');
		
		this.FG_Awal = this.add.image(/2,500/2, 'FG_Awal');
		this.FG_Awal.setDepth(10);
		this.FG_Awal.y -= 768;

		this.tweens.add({
			targets: this.FG_Awal,
			duration: 750,
			y: 768/2
		});

		this.BMulai = this.add.image(432/2,768/2, 'BMulai');
		this.BMulai.setDepth(10);
		this.BMulai.setScale (1);
		this.BMulai.setInteractive();

		this.tweens.add({
			targets: this.BMulai,
			ease: 'Elastic',
			duration: 1000,
			delay: 1500,
			scaleX: 1,
			scaleY: 1
		});

		this.Title = this.add.image(432/2, 100, 'Title');
		this.Title.setDepth(10);
		this.Title.setScale(0);

		this.tweens.add({
			targets: this.Title,
			ease: 'Elastic',
			duration: 1500,
			delay: 1000,
			scaleX: 1,
			scaleY: 1
		});
		this.Chara = this.add.image(50, 500,'Chara');
		this.Chara.setDepth(3);
		this.Chara.setVisible(false);
		this.Chara.setScale(1);
		this.Chara.setInteractive();

		this.Chara1 = this.add.image(200,500, 'Chara1');
		this.Chara1.setDepth(3);
		this.Chara1.setVisible(false);
		this.Chara1.setScale(1);
		this.Chara1.setInteractive();

		this.Chara2 = this.add.image(350,500,'Chara2');
		this.Chara2.setDepth(3);
		this.Chara2.setVisible(false);
		this.Chara2.setScale(1);
		this.Chara2.setInteractive();

		this.isGameRunning = false;

		this.time.delayedCall(0, this.startInputEvent, [], this);
	}
//=====#MAIN=====//
}); 

// Konfigurasi scene
var config = {
	type: Phaser.AUTO,
	width: 432,
	height: 768,
	scene: [ScenePlay]
};

// Membuat & Menjalankan objek game sesuai konfigurasi yang telah dibuat (oleh Phaser)
var game = new Phaser.Game(config);

// Fungsi untuk menyesuaikan ukuran layar agar tidak terpotong 
function resize() {
	var canvas = document.querySelector("canvas");
	var windowWidth = window.innerWidth;
	var windowHeight = window.innerHeight;
	var windowRatio = windowWidth / windowHeight; 

	// game = variable dari new PhaserGame(config);
	var gameRatio = game.config.width / game.config.height;

	if (windowRatio < gameRatio){
		canvas.style.width = windowWidth + "px";
		canvas.style.height = (windowWidth / gameRatio) + "px";
	} else {
		canvas.style.width = (windowHeight * gameRatio) + "px";
		canvas.style.height = windowHeight + "px";
	}
}

window.onload = function(){
	resize();
	window.addEventListener("resize", resize, false);
};